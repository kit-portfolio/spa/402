import React from 'react';
import './theme/common.scss'
import './theme/normalize.scss';
import MainMenu from "./sections/main-menu/main-menu";

function App() {
  return (
    <div className="app app-default-background">
      <MainMenu/>
    </div>
  );
}

export default App;


// TODO: add share media
// TODO: error boundary
