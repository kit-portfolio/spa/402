# [402] - Drag Racing Club
:construction: UNDER CONSTRUCTION  :construction:

### TECH
* Typescript
* React 
* Redux
* Router 
* SaSS

### DEMO
Live demo is hosted [here](https://kit-portfolio.gitlab.io/spa/402/)